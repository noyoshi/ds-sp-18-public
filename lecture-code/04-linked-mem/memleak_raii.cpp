// this will be raii compliant
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <iostream>
#include <vector>

using namespace std;

const int NITEMS = 1<<10;
const int TRIALS = 100;

class IntArray {
public:
    typedef int* iterator;

    IntArray(const int n)               { length = n; data = new int[n];  } // for RAII, must have constructor
    ~IntArray()                         { delete [] data; } // for RAII, must have destructor
    const size_t size() const           { return length; }
    int& operator[](const int i)        { return data[i]; }
    iterator begin()                    { return data; }
    iterator end()                      { return data + length; }

private:
    size_t  length;
    int*    data;
};


bool duplicates(int n){

    //int *randoms = new int[n];
    IntArray randoms(n);
    bool result = true;

    // creating an array with random input
    for (size_t i = 0; i < NITEMS; i++) {
        randoms[i] = rand() % 1000;
    }

    for (size_t i = 0; i < n; i++) {
        if (find(randoms.begin(), randoms.end(), randoms[i]) != randoms.end()){
        //if (find(randoms + i + 1, randoms + NITEMS, randoms[i]) != (randoms + NITEMS)){
            return true; // duplicates found
            //goto failure; // remove this line.
        }
    }
    return false;
}

int main(int argc, char const *argv[]) {
    srand(time(NULL)); // seeding the rand

    for (size_t i = 0; i < TRIALS; i++) {
        if (duplicates(NITEMS)) {
            cout << "duplicates detected! " << i << endl;
        } else {
            cout << "no dupes. " << i << endl;
        }
    }
    return 0;
}

// run valgrind with options:
//valgrind --leak-check=full --show-leak-kinds=all ./a.out
